package org.csystem.datetimepoc;

public class MyDateParser {
    public static MyDate deseri(String dateBugün) {
        MyDate myDate = new MyDate();

        // "2020-02-30"
        String[] tokens = dateBugün.split("-");

        String yearFromToken = tokens[0]; // 2020*09
        String month = tokens[1];
        String day = tokens[2];

        myDate.year = Integer.parseInt("2020");
        myDate.month = Integer.parseInt(month);;
        myDate.day = Integer.parseInt(day);;

        return myDate;
    }
}

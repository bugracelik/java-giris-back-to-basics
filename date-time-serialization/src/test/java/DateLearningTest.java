import org.csystem.datetimepoc.MyDate;
import org.csystem.datetimepoc.MyDateParser;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.Temporal;

public class DateLearningTest {

    @Test
    public void now() {
        LocalDate now = LocalDate.now();
        System.out.println(now);
    }

    @Test
    void kaybolmasın_diye_koymaca() {

        System.out.println("poc");

        int year = 2020;
        int month = 9;
        int day = 11;

        LocalDate date = LocalDate.of(year, month, day); // of creation factory method

        String dateBugün = "2020-09-11";

        MyDate mydate = MyDateParser.deseri("2020-09-11"); // serialization  object mapper tojson yazmak zordu yeni field
        System.out.println(mydate);

        LocalDate parsedDate = LocalDate.parse(dateBugün); // deserialization string'e modele
        System.out.println(parsedDate.getYear());
        parsedDate.getDayOfYear();

        // System.out.println(pa);
    }

    @Test
    void dilin_temporal_bekledigi_yere_local_date_gecmece() {
        Temporal temporal = LocalDate.now();
    }

    @Test
    void kullanmaca() {
        Serializable temporal = LocalDate.now();
    }
}

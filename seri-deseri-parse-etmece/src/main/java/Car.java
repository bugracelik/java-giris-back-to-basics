public class Car {
    private String model;
    private String yıl;
    private String renk;
    private Integer kapi_sayisi;

    public Car(String model, String yıl, String renk, Integer kapi_sayisi) {
        this.model = model;
        this.yıl = yıl;
        this.renk = renk;
        this.kapi_sayisi = kapi_sayisi;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYıl() {
        return yıl;
    }

    public void setYıl(String yıl) {
        this.yıl = yıl;
    }

    public String getRenk() {
        return renk;
    }

    public void setRenk(String renk) {
        this.renk = renk;
    }

    public Integer getKapi_sayisi() {
        return kapi_sayisi;
    }

    public void setKapi_sayisi(Integer kapi_sayisi) {
        this.kapi_sayisi = kapi_sayisi;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", yıl='" + yıl + '\'' +
                ", renk='" + renk + '\'' +
                ", kapi_sayisi=" + kapi_sayisi +
                '}';
    }
}

class CarDto {
    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}


class CarMapperSeri {

    public static CarDto map(Car car) {
        CarDto carDto = new CarDto();

        carDto.setModel(car.getModel());

        // object mapper seri etmece

        return carDto;
    }
}



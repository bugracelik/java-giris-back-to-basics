import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SeriDeseriParseEtmeceImpl implements SeriDeseriParseEtmece {

    public static void main(String[] args) throws JsonProcessingException {
        SeriDeseriParseEtmeceImpl s = new SeriDeseriParseEtmeceImpl();
        s.herhangi_bir_modeli_once_seri_edelim();
    }

    public void bir_tane_object_mapper_alınır(){
        ObjectMapper mapper = new ObjectMapper();
    }

    public void library_bulunur() {
        System.out.println("ok library maven'dan");
    }

    public void herhangi_bir_modeli_once_seri_edelim() throws JsonProcessingException {
        Model model = new Model();
        Car car = new Car("opel", "2020", "kırmızı", 4);
        model.setName("sss"); // heapte nesne model name ide bugra 1 tane

        ObjectMapper mapper = new ObjectMapper();
        String modelJsonVersionStr = mapper.writeValueAsString(model); // modeli json'a seri et, stringe seri et. model to string aşağı seri etmece
        System.out.println(modelJsonVersionStr);
        String carJsonVersionStr = mapper.writeValueAsString(car);
        System.out.println(carJsonVersionStr);
    }
}


class Model {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
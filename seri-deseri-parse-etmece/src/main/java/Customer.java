

public class Customer {


    private String name;
    private String surname;
    private Integer year;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int[] getAsal()
    {
        return new int[]{2, 3, 5, 7};
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }
}

//dto için oluşturdugum sınıf
class CustomerDto{
    private String name;
    private Integer year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "CustomerDto{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}

class CustomerMapperSeri{
    public static CustomerDto map(Customer customer){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setName(customer.getName());
        customerDto.setYear(customer.getYear());
        return customerDto;
    }
}
class Foo{
    public static void main(String[] args) {
        Customer customer = new Customer();
        customer.setName("bugra");
        customer.setYear(21);
        customer.setSurname("çelik");
        CustomerDto map = CustomerMapperSeri.map(customer);
        String s = map.toString();
        System.out.println(s);

    }

}

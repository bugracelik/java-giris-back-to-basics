import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SeriDeseriParseEtmeceTest {


    @Test
    void string_compare() {
        String baba = "BABA";
        String baba1 = "BABA";

        if (baba.equals(baba1)) {
            System.out.println("eşit");
        }
        else System.out.println("eşit değiş");

        // varsayımlar
        Assertions.assertEquals(2, 2);
    }

    @Test
    void name() {
        // given
        Customer c = new Customer();

        // when
        c.setName("bugra");

        // then
        //Assertions.assertEquals(null, c.getName());

        // given
        int[] asalExpected = {2, 3, 5, 7};

        // when
        int[] asal = c.getAsal();

        // then
        Assertions.assertArrayEquals(asalExpected, asal);



    }

    @Test
    void bir_tane_object_mapper_alınır() throws Exception {
        // given
        String expectedJson = "{\"model\":\"audi\",\"yıl\":\"2020\",\"renk\":\"mavi\",\"kapi_sayisi\":2}";
        ObjectMapper objectMapper = new ObjectMapper();
        Car car = new Car("audi", "2020", "mavi", 2);

        // when
        String strVersionJsonCar = objectMapper.writeValueAsString(car); // seri etmece

        // then
        Assertions.assertNotEquals(expectedJson, strVersionJsonCar);
    }

    @Test
    void library_bulunur() {
    }

    @Test
    void herhangi_bir_modeli_once_seri_edelim() {
    }
}
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;


class CustomerTest {

    /*
     *Customer sınıfınının içerisinde kullanılan metodların
     * test'leriniz yazıyoruz bu sayede Customer.class da
     * yazılan tüm metodların dogru çalışıp çalışmadıgını
     * kontol ediyoruz.
     */

    @Test
    void setName() {
        //given
        Customer customer = new Customer();

        //when
        customer.setName("bugra");

        //then
        Assertions.assertEquals("bugra", customer.getName() );

    }

    @Test
    void getAsal() {
        //given
        int [] asal = {2, 3, 5, 7};

        //when
        Customer customer = new Customer();
        int[] asal1 = customer.getAsal();

        //then
        Assertions.assertArrayEquals(asal, asal1);
    }

    @Test
    void testToString() {
        //given
        Customer customer = new Customer();
        customer.setName("ali riza");
        customer.setSurname("keskin");
        customer.setYear(50);
        String exceptedToString = "Customer{name='ali riza', surname='keskin', year=50}";

        //when
        String customerToString = customer.toString();

        //then
        Assertions.assertEquals(exceptedToString, customerToString);

    }

    @Test
    void getSurname() {
        //given
        Customer customer = new Customer();
        customer.setSurname("çelik");
        String exceptedSurname = "çelik";

        //when
        String surname = customer.getSurname();

        //then
        Assertions.assertEquals(exceptedSurname, surname);



    }

    @Test
    void setSurname() {
        //given
        Customer customer = new Customer();
        customer.setSurname("eren");
        String exceptedSurname = "eren";

        //when
        String surname = customer.getSurname();

        //then
        Assertions.assertEquals(exceptedSurname, surname);
    }

    @Test
    void getYear() {
        //given
        Customer customer = new Customer();
        customer.setYear(10);
        Integer exceptedYear = 10;

        //when
        Integer year = customer.getYear();

        //then
        Assertions.assertEquals(exceptedYear, year);
    }

    @Test
    void setYear() {
        //given
        Customer customer = new Customer();
        Integer exceptedYear = 10;

        //when
        customer.setYear(10);
        Integer year = customer.getYear();

        //then
        Assertions.assertEquals(exceptedYear, year);
    }

    @Test
    void testSetName() {
        //given
        Customer customer = new Customer();
        String exceptedName = "sümeyye";

        //when
        customer.setName("sümeyye");
        String name = customer.getName();

        //then
        Assertions.assertEquals(exceptedName, name);

    }

    @Test
    void getName() {
        //given
        Customer customer = new Customer();
        String exceptedName = "alper";

        //when
        customer.setName("alper");
        String name = customer.getName();

        //then
        Assertions.assertEquals(exceptedName, name);

    }


}